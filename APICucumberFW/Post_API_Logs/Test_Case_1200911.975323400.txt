Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "<NAME>",
    "job": "<JOB>"
}

Response header date is : 
Sat, 16 Mar 2024 14:39:12 GMT

Response body is : 
{"name":"<NAME>","job":"<JOB>","id":"722","createdAt":"2024-03-16T14:39:12.109Z"}