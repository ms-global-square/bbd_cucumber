package Repository;

import java.io.IOException;
import java.util.ArrayList;

import Common_Methods.Utility;

public class RequestBody extends Environment {
public static String req_tc1() throws IOException {
ArrayList<String> Data=Utility.readExcelData("Post_API", "Post_TC1");
		String key_name = Data.get(1);
		String value_name= Data.get(2);
		String key_job= Data.get(3);
		String value_job = Data.get(4);
String req_body = "{\r\n" + "    \""+key_name+"\": \""+value_name+"\",\r\n" + "    \""+key_job+"\": \""+value_job+"\"\r\n" + "}";
		return req_body;
	}
	
	public static String req_tc2() throws IOException {
		ArrayList<String> Data=Utility.readExcelData("Post_API", "Post_TC2");
		String key_name = Data.get(1);
		String value_name= Data.get(2);
		String key_job= Data.get(3);
		String value_job = Data.get(4);
		String req_body = "{\r\n" + "    \""+key_name+"\": \""+value_name+"\",\r\n" + "    \""+key_job+"\": \""+value_job+"\"\r\n" + "}";
		return req_body;
	}

public static String req_tc5() throws IOException {
ArrayList<String> Data=Utility.readExcelData("Post_API", "Post_TC5");
		String key_name = Data.get(1);
		String value_name= Data.get(2);
		String key_job= Data.get(3);
		String value_job = Data.get(4);
String req_body = "{\r\n" + "    \""+key_name+"\": \""+value_name+"\",\r\n" + "    \""+key_job+"\": \""+value_job+"\"\r\n" + "}";
		return req_body;
	}
}


















