package cucumber.Options;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/features", glue = { "stepDefinations" }, tags= "@Put_API_TestCases or @Post_API_Testcases or @DataDriven or @Patch_API_Testcases or @Delete_Testcases")
public class TestRunner {
	
}
