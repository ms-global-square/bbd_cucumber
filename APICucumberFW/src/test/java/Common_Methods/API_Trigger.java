package Common_Methods;

import java.io.IOException;

import Repositories.Environment;
import Repositories.RequestBody;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public abstract class API_Trigger {

	public static Response Post_trigger(String HeaderName, String HeaderValue, String reqbody, String Endpoint) {

		RequestSpecification requestSpec = RestAssured.given();

		requestSpec.header(HeaderName, HeaderValue);

		requestSpec.body(reqbody);

		Response response = requestSpec.post(Endpoint);

		return response;

	}


public static void post_reg_succ_trigger(String headername, String headervalue, String requestbody,
		String endpoint) throws IOException {
	RequestSpecification req_spec = RestAssured.given();
	req_spec.header(Environment.HeaderName(), Environment.HeaderValue());
	req_spec.body(requestbody);
	requestbody = RequestBody.req_tc1();

}

public static Response get_list_user_trigger(String headername, String headervalue, String endpoint) {
	RequestSpecification req_spec = RestAssured.given();
	req_spec.header(Environment.HeaderName(), Environment.HeaderValue());
	Response response = req_spec.put(Environment.Hostname() + Environment.Hostname());
	return response;
}

public static Response Put_trigger(String HeaderName, String HeaderValue, String reqbody, String Endpoint) {

	RequestSpecification requestSpec = RestAssured.given();

	requestSpec.header(HeaderName, HeaderValue);

	requestSpec.body(reqbody);

	Response response = requestSpec.put(Endpoint);

	return response;

}


public static Response Patch_trigger(String HeaderName, String HeaderValue, String reqbody, String Endpoint) {

	RequestSpecification requestSpec = RestAssured.given();

	requestSpec.header(HeaderName, HeaderValue);

	requestSpec.body(reqbody);

	Response response = requestSpec.put(Endpoint);

	return response;

}

public static Response Delete_trigger(String Endpoint) {

	RequestSpecification requestSpec = RestAssured.given();

	Response response = requestSpec.delete(Endpoint);

	return response;
}

}

