Feature: trigger the DELETE API to remove the resource

@Delete_Testcases
Scenario: trigger the delete api 
         Given configure the endpoint of delete_api
         When trigger the api to hit the endpoint of delete_api
         Then validate status code for delete_api

