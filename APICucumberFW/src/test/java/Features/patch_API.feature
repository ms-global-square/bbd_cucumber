Feature: trigger the PATCH API to update the user

@Patch_API_Testcases
Scenario: trigger the API with valid request body parameters
         Given enter valid name and job in requestbody of patch_api
         When trigger the api to hit the endpoint of patch_api
         Then validate status code for patch_api
         And validate response body parameters of patch_api

