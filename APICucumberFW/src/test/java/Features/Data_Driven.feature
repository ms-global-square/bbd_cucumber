Feature: Trigger post API on the basis of Input Data

@DataDriven
Scenario Outline:Trigger the post API request with valid request parameters
       Given Enter "<NAME>" and "<JOB>" in request body
       When Send the request with payload
       Then Validate status code 
       And Validate response body parameters
       
Examples:

      |Name |Job |
      |Pooja |Eng |
      |Shapur|QA |
       