package stepDefinations;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;


import Repository.Environment;
import Repository.RequestBody;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Put_API_update {

	int Statuscode;
	String endpoint;
	String requestbody;
	File dir_name;
	Response response;
	String res_name;
	String res_job;
	String res_time;
	String exp_time;
	String req_name;
	String req_job;
    String Endpoint;
    
    @Before("@Put_API_TestCases")
    public void beforeScenariopost() throws IOException{
		dir_name = Utility.CreateLogDirectory("Post_API_Logs");
		requestbody = RequestBody.req_tc1();
		endpoint = Environment.Hostname() + Environment.Resource();
    }
    
    @Given("enter valid name and job in put_api requestbody")
    public void enter_valid_name_and_job_in_put_api_requestbody() throws IOException {
		Endpoint =  Environment .Hostname() +  Environment.Resource_Put();
		requestbody = RequestBody.req_tc2();
		System.out.println(requestbody );
		dir_name = Utility.CreateLogDirectory("put_update_API_log");
		response = API_Trigger.Put_trigger(Environment.HeaderName(), Environment.HeaderValue(), requestbody,
				Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("put_update"), dir_name, endpoint, requestbody,
				response.getHeader("Date"), response.getBody().asPrettyString());
//	    throw new io.cucumber.java.PendingException();
	}

    @When("trigger the api to hit the put_api endpoint")
	public void trigger_the_api_to_hit_the_put_api_endpoint() {
		Statuscode = response.getStatusCode();
		System.out.println("\n" + "status code is:" + " " + Statuscode);
		System.out.println("\n" + "-----response body is ------");

		System.out.println(response.asPrettyString());
//		create request body object to fetch request body parameters
		JsonPath req_jsn = new JsonPath(requestbody);
		System.out.println("\n" + "-----request body parameters-------");
		req_name = req_jsn.getString("name");
		System.out.println("name:" + req_name);
		req_job = req_jsn.getString ("job");
		System.out.println("job:" + req_job);

//		extract response body parameters
		System.out.println("\n" + "-------response body parameters-------");

		res_name = response.getBody().jsonPath().getString("name");
		System.out.println("name:" + res_name);
		res_job = response.getBody().jsonPath().getString("job");
		System.out.println("job:" + res_job);
		res_time = response.getBody().jsonPath().getString("updatedAt");
		System.out.println("updated at:" + res_time);
		res_time = res_time.substring(0, 11);
		System.out.println(res_time);

//		get local time
		LocalDateTime curranttime = LocalDateTime.now();
		exp_time = curranttime.toString().substring(0, 11);

//	    throw new io.cucumber.java.PendingException();
	}

    @Then("validate status code of put_api")
    public void validate_status_code_of_put_api() {
		Assert.assertEquals(Statuscode, 200);
//	    throw new io.cucumber.java.PendingException();
	}

    @Then("validate response body parameters of put_api")
    public void validate_response_body_parameters_of_put_api() {
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_time, exp_time);
//	    throw new io.cucumber.java.PendingException();
	}
}




