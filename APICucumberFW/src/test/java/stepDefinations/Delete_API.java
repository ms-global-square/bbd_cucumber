package stepDefinations;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repositories.RequestBody;
import Repository.Environment;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Delete_API {
	
	String endpoint;
	File dir_name;
	Response response;
	int Stauscode;
    String Endpoint=Environment.Hostname()+Environment.Resource_Del();
    
    @Before("@Delete_API_Testcases")
    public void beforeScenariopost() throws IOException{
		dir_name = Utility.CreateLogDirectory("Post_API_Logs");
		Endpoint = RequestBody.req_tc1();
		Endpoint = Environment.Hostname() + Environment.Resource();
    }
	@Given("configure the endpoint of delete_api")
	public void configure_the_endpoint_of_delete_api()
{
	
		Response response = API_Trigger.Delete_trigger(Endpoint);
		Stauscode = response.statusCode();
		System.out.println(Stauscode);
		}

	@When("trigger the api to hit the endpoint of delete_api")
	public void trigger_the_api_to_hit_the_endpoint_of_delete_api() {
	

		System.out.println("status code is :" + Stauscode);
//	    throw new io.cucumber.java.PendingException();
	}

	@Then("validate status code for delete_api")
	public void validate_status_code_for_delete_api() {
		Assert.assertEquals(Stauscode, 204);
//	    throw new io.cucumber.java.PendingException();
	}

}



