package stepDefinations;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repositories.RequestBody;
import Repository.Environment;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class PoststepDefination {
	Response response;
	int statuscode;
	String res_name;
	String res_job;
	String res_id;
	String res_createdAt;
	String requestBody;
	String req_name;
	String req_job;
	String expecteddate;
	File dir_name;
	String Endpoint;
	
	@Before("@Post_API_Testcases")
    public void beforeScenariopost() throws IOException{
		dir_name = Utility.CreateLogDirectory("Post_API_Logs");
		requestBody = RequestBody.req_tc1();
		Endpoint = Environment.Hostname() + Environment.Resource();
    }


	@Given("Enter {string} and {string} in request body")
	public void enter_and_in_request_body(String req_name, String req_job ) throws IOException {
		dir_name = Utility.CreateLogDirectory("Post_API_Logs");
		requestBody = "{\r\n" + "    \"name\": \"" + req_name + "\",\r\n" + "    \"job\": \"" + req_job + "\"\r\n"
				+ "}";
		Endpoint = Environment.Hostname() + Environment.Resource();
		response = API_Trigger.Post_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(), requestBody, Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("Post_TC1"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
		statuscode = response.statusCode();

	}

	@Given("Enter NAME and JOB in request body")
	public void enter_name_and_job_in_request_body() throws IOException {
		dir_name = Utility.CreateLogDirectory("Post_API_Logs");
		requestBody = RequestBody.req_tc1();
		Endpoint = Environment.Hostname() + Environment.Resource();
		response = API_Trigger.Post_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(), requestBody, Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("Post_TC1"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
	}

     @When("Send the request with payload")
     public void send_the_request_with_payload() {
		statuscode = response.statusCode();
		ResponseBody res_body = response.getBody();
		res_name = res_body.jsonPath().getString("name");
		res_job = res_body.jsonPath().getString("job");
		res_id = res_body.jsonPath().getString("id");
		res_createdAt = res_body.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.substring(0, 11);
		JsonPath jsp_req = new JsonPath(requestBody);
	    req_name = jsp_req.getString("name");
		req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		expecteddate = currentdate.toString().substring(0, 11);
		//throw new io.cucumber.java.PendingException();
	}

	@Then("Validate status code")
	public void validate_status_code() {
		Assert.assertEquals(statuscode, 201);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate response body parameters")
	public void validate_response_body_parameters() {

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt, expecteddate);
		// throw new io.cucumber.java.PendingException();
	}

}
