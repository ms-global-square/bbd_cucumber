package stepDefinations;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;

import Repository.Environment;
import Repository.RequestBody;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Post_Reg_Succ_Description {

	File dir;
	String endpoint;
	String requestbody;
	Response response;
	int status_code;
	String req_email;
	String req_pasw;
	String res_id;
	String res_token;
	
	@Before("@Post_API_Testcases")
    public void beforeScenariopost() throws IOException{
		dir = Utility.CreateLogDirectory("Post_API_Logs");
		requestbody = RequestBody.req_tc1();
		endpoint = Environment.Hostname() + Environment.Resource();
    }


	@Given("enter email and password in requestbody")
	public void enter_email_and_password_in_requestbody() throws IOException {
		dir = Utility.CreateLogDirectory("post_reg_succ_log");
		endpoint = Environment.Hostname() + Environment.Resource_Reg();
		requestbody = RequestBody.req_tc5();

		response = API_Trigger.Post_trigger(Environment.HeaderName(), Environment.HeaderValue(),
				requestbody, endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("post_reg_succ"), dir, endpoint, requestbody,
				response.getHeader("Date"), response.getBody().asPrettyString());

//	    throw new io.cucumber.java.PendingException();

	}

	@When("trigger the API with given payload")
	public void trigger_the_api_with_given_payload() {
		System.out.println(response.getBody().asPrettyString());

//		fetch status code and store i to variable
		status_code = response.getStatusCode();
		System.out.println("status code is :" + status_code);

//			fetch request body parameters 

		System.out.println("------request body parameters--------");
		JsonPath req_jsn = new JsonPath(requestbody);
		req_email = req_jsn.getString("email");
		System.out.println(req_email);
		req_pasw = req_jsn.getString("password");
		System.out.println(req_pasw);

//		fetch response body

		System.out.println("-------response body parameters--------");
		res_id = response.getBody().jsonPath().getString("id");
		System.out.println(res_id);
		res_token = response.getBody().jsonPath().getString("token");
		System.out.println(res_token);
//	    throw new io.cucumber.java.PendingException();

	}

	@Then("validate ststus code")
	public void validate_ststus_code() {
		Assert.assertEquals(status_code, 200);
//	    throw new io.cucumber.java.PendingException();
	}

	@Then("validate response body parameters")
	public void validate_response_body_parameters() {
		Assert.assertNotNull(res_id);
		Assert.assertNotNull(res_token);
//	    throw new io.cucumber.java.PendingException();
	}
}


