Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Fri, 15 Mar 2024 18:11:28 GMT

Response body is : 
{
    "name": "morpheus",
    "job": "leader",
    "id": "845",
    "createdAt": "2024-03-15T18:11:28.352Z"
}