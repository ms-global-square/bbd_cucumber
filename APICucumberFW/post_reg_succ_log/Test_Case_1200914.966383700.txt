Endpoint is :
https://reqres.in/api/register

Request body is :
{
    "email": "eve.holt@reqres.in",
    "password": "pistol"
}

Response header date is : 
Sat, 16 Mar 2024 14:39:15 GMT

Response body is : 
{
    "id": 4,
    "token": "QpwL5tke4Pnpja7X4"
}