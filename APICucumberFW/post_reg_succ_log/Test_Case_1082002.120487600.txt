Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Sat, 16 Mar 2024 02:50:02 GMT

Response body is : 
{
    "name": "morpheus",
    "job": "leader",
    "id": "250",
    "createdAt": "2024-03-16T02:50:02.069Z"
}