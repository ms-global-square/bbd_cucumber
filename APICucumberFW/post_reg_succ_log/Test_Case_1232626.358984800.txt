Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Fri, 15 Mar 2024 17:56:27 GMT

Response body is : 
{
    "name": "morpheus",
    "job": "leader",
    "id": "956",
    "createdAt": "2024-03-15T17:56:27.175Z"
}