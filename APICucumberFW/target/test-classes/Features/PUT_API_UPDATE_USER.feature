Feature: Trigger the PUT API to update the user

@Put_API_TestCases
Scenario: Trigger the api with valid request parameters
         Given enter valid name and job in put_api requestbody
         When trigger the api to hit the put_api endpoint
         Then validate status code of put_api
         And validate response body parameters of put_api

